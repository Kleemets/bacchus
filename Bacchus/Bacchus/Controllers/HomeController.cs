﻿using Bacchus.Mapper;
using Bacchus.Models;
using Bacchus.Service;
using Bacchus.ViewModel;
using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace Bacchus.Controllers
{
	public class HomeController : Controller
	{
		private AuctionrRESTApiModel _auctionRest;
		private IndexViewModel _indexVm;
		private ShowOfferViewModel _showOfferVm;
		private OfferViewModel _offerVm;
		public HomeController() {
			_auctionRest = new AuctionrRESTApiModel();
			_indexVm = new IndexViewModel();
			_showOfferVm = new ShowOfferViewModel();
			_offerVm = new OfferViewModel();
		}
		public ActionResult Index(string listOfAllCategories)
		{
			_indexVm.ListOfAllAuctions = _auctionRest.GetAllAuctions().Result;
			var listOfCategories = _indexVm.ListOfAllAuctions.Select(x => x.ProductCategory).Distinct();
			ViewBag.listOfAllCategories = new SelectList(listOfCategories);

			if (string.IsNullOrEmpty(listOfAllCategories))
			{
				return View("Index", _indexVm);
			}
			else {
				_indexVm.ListOfAllAuctions = _indexVm.ListOfAllAuctions.Where(a => a.ProductCategory == listOfAllCategories).ToList();
				return View("Index", _indexVm);
			}
		}

		public ActionResult ShowOffers() {
			if (Session["Username"] != null)
			{
				Models.User user = UserService.FindUser(Session["Username"].ToString());
				List<Offer> offers = OfferService.FindAllBidsByUserId(user.UserId);
				_showOfferVm.Offers = offers;
				return View(_showOfferVm);
			}
			else {
				return RedirectToAction("Index");
			}
			
		}
	

		public ActionResult MakeOffer(Guid? id)
		{
			Auction auction = _auctionRest.GetAllAuctions().Result.First(a => a.ProductId == id.Value);
			_offerVm.Auction = auction;
			return View(_offerVm);
		}
		[HttpPost]
		public ActionResult MakeOffer(OfferViewModel vm)
		{
			Models.User user = UserService.FindUser(vm.User.UserName);
			if (user != null)
			{
				SaveOffer(vm, user);
			}
			else {
				Models.User newUser = UserService.SaveUser(vm.User);
				SaveOffer(vm, newUser);
			}
			return RedirectToAction("Index");
		}

		private void SaveOffer(OfferViewModel vm, User user)
		{
			Session["Username"] = user.UserName;
			vm.User = user;
			OfferMapper mapper = new OfferMapper();
			var offer = mapper.MapOffer(vm);
			OfferService.SaveOffer(offer);
		}
	}
}