﻿using Bacchus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bacchus.ViewModel
{
	public class OfferViewModel
	{
		public Auction Auction { get; set; }
		public User User { get; set; }
		public Offer Offer { get; set; }
	}
}