﻿using Bacchus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bacchus.ViewModel
{
	public class ShowOfferViewModel
	{
		public List<Offer> Offers { get; set; }
	}
}