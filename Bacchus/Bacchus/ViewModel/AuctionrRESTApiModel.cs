﻿using Bacchus.Models;
using Bacchus.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bacchus.ViewModel
{
	public class AuctionrRESTApiModel
	{
		private AuctionService _auctionService;

		public AuctionrRESTApiModel() {
			_auctionService = new AuctionService();
		}

		public async Task<List<Auction>> GetAllAuctions() {
			var code = Task.Run(() => _auctionService.GetAllAuctions());
			code.Wait();
			return await code;
		}

	}
}