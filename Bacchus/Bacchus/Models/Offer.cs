﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bacchus.Models
{
	public class Offer
	{
		public string Bid { get; set; }
		public Guid ProductId { get; set; }
		public string ProductName { get; set; }
		public string ProductDescription { get; set; }
		public string ProductCategorys { get; set; }
		public DateTime BidDate { get; set; }
		public Guid UserId { get; set; }
		public string LastBidDate { get; set; }
	}
}