﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bacchus.Models
{
	public class User
	{
		public string UserName { get; set; }
		public DateTime CreatedDate { get; set; }

		public Guid UserId { get; set; }

	}
}