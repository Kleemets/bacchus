﻿using Bacchus.Models;
using Bacchus.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bacchus.Mapper
{
	public class OfferMapper
	{
		public Offer MapOffer(OfferViewModel vm) {
			Offer offer = new Offer()
			{
				Bid = vm.Offer.Bid,
				ProductId = vm.Auction.ProductId,
				BidDate = DateTime.Now,
				UserId = vm.User.UserId
				
			};
			return offer;
		}

		public Offer MapDomainToModel(Domain.Offer offer) {
			Offer modelOffer = new Offer() {
				Bid = offer.OfferedPrice,
				BidDate = offer.OfferDateTime,
				ProductId = offer.ProductId,
				UserId = offer.UserId
			};

			return modelOffer;
		}
	}
}