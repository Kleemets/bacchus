﻿using Bacchus.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bacchus.Mapper
{
	public class UserMapper
	{
		public Models.User MapUser(User user) {
			Models.User modelsUser = new Models.User()
			{
				UserId = user.Id,
				UserName = user.UserName,
				CreatedDate = user.CreatedTime
			};

			return modelsUser;
		}
	}
}