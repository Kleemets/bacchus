﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Bacchus.Service
{
	public class BaseService
	{
		protected HttpClient client;

		public BaseService(string uri)
		{
			client = new HttpClient();
			client.BaseAddress = new Uri(uri);
		}

		//GET DATA
		protected async Task<T> GetData<T>(string urlPath)
		{
			//Added Reference to solution System.Web.

			var responseMessage = await client.GetAsync(urlPath);

			responseMessage.EnsureSuccessStatusCode();
			T ret = await responseMessage.Content
					.ReadAsAsync<T>();

			return ret;
		}

	}
}