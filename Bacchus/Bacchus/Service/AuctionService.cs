﻿using Bacchus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Bacchus.Service
{
	public class AuctionService : BaseService
	{
		public AuctionService() : base("http://uptime-auction-api.azurewebsites.net/api/Auction")
		{
		}
		public async Task<List<Auction>> GetAllAuctions()
		{
			return await base.GetData<List<Auction>>("");
		}

	}
}