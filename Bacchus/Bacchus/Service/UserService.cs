﻿using Bacchus.Domain;
using Bacchus.Mapper;
using Bacchus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Bacchus.Service
{
	public class UserService
	{
		public static Models.User FindUser(string username) {
			using (BacchusDBEntities db = new BacchusDBEntities()) {
				var foundUser = db.Users.FirstOrDefault(user => user.UserName == username.Trim());
				if (foundUser == null) {
					return null;
				}
				UserMapper mapper = new UserMapper();
				return mapper.MapUser(foundUser);
			}
		}
		internal static Models.User SaveUser(Models.User user)
		{
			using (BacchusDBEntities db = new BacchusDBEntities()) {
				Domain.User domainUser = new Domain.User()
				{
					Id = Guid.NewGuid(),
					CreatedTime = DateTime.Now,
					UserName = user.UserName.Trim(),
					Deleted = false
				};
				user.UserId = domainUser.Id;
				db.Users.Add(domainUser);
				db.SaveChanges();
			}
			
			return user;

		}
	}
}