﻿using Bacchus.Domain;
using Bacchus.Mapper;
using Bacchus.Models;
using Bacchus.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bacchus.Service
{
	public class OfferService
	{
		public static void SaveOffer(Bacchus.Models.Offer bid) {
			using (BacchusDBEntities db = new BacchusDBEntities()) {
				Domain.Offer offer = new Domain.Offer()
				{
					UserId = bid.UserId,
					Deleted = false,
					Id = Guid.NewGuid(),
					ProductId = bid.ProductId,
					OfferedPrice = bid.Bid,
					OfferDateTime = DateTime.Now
				};

				db.Offers.Add(offer);
				db.SaveChanges();
			};
		}

		internal static List<Models.Offer> FindAllBidsByUserId(Guid userId)
		{
			using (BacchusDBEntities db = new BacchusDBEntities())
			{
				CancelAllOutDatedAuctions(userId);
				OfferMapper mapper = new OfferMapper();
				List<Domain.Offer> domianOffers=  db.Offers.Where(o => o.UserId == userId && o.Deleted == false).ToList();
				List<Models.Offer> listOfOffers = new List<Models.Offer>();
				foreach (Domain.Offer offer in domianOffers)
				{
					listOfOffers.Add(mapper.MapDomainToModel(offer));
				}
				AuctionrRESTApiModel auctionService = new AuctionrRESTApiModel();
				List<Auction> allAuctions = auctionService.GetAllAuctions().Result;

				foreach (Models.Offer offer in listOfOffers)
				{
					Auction auction =allAuctions.FirstOrDefault(a => a.ProductId == offer.ProductId);
					if (auction != null)
					{
						offer.LastBidDate = auction.BiddingEndDate.Subtract(DateTime.Now).ToString();
						offer.ProductName = auction.ProductName;
						offer.ProductDescription = auction.ProductDescription;
						offer.ProductCategorys = auction.ProductCategory;
					}
				}
				return listOfOffers;
			};
		}
		internal static void CancelAllOutDatedAuctions(Guid userId) {
			using (BacchusDBEntities db = new BacchusDBEntities())
			{
				AuctionrRESTApiModel auctionService = new AuctionrRESTApiModel();
				List<Auction> allAuctions = auctionService.GetAllAuctions().Result;
				List<Domain.Offer> domianOffers = db.Offers.Where(o => o.UserId == userId).ToList();
			
				foreach (Domain.Offer offer in domianOffers)
				{
					Auction auction = allAuctions.FirstOrDefault(a => a.ProductId == offer.ProductId);
					if (auction == null) {
						offer.Deleted = true;
					}
					
					db.SaveChanges();
				}
			};
		}
	}
}