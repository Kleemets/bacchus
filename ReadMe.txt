Installation Guide:
- git clone project from this link ---> https://Kleemets@bitbucket.org/Kleemets/bacchus.git
- open the "Bacchus" folder where did you "git clone" it
- find file Bacchus.sln and open it with Visual Studio.
- go to the route of the Project and right-click on the "Solution 'Bacchus' (1 project)" and select "Build solution"
- go to the Navigation bar and select "Debug" and from there "Start Without Debugging"
- Web Application should start in your browser

---Rakenduse kohta k�ivad m�rkused----
Kuna ei lugenud juhendist v�lja, kas on vajalik teha eraldi sisselogimise vaade ka, siis tegin rakenduse sessioonile p�hineva.
Kui kasutaja tuleb esimest korda veebirakendusse, siis ei kuvata talle �htegi pakkumist. Kui kasutaja teeb esimese pakkumise, siis 
kuvatakse talle "My Bids" men��s tema pakkumised. Kui n��d sama kasutaja teeb uue pakkumise teise nimega, siis kuvatakse talle "My Bids"
men��s ainult viimase kasutajanimega tehtud pakkumised.
Samuti j�i REST API poolt pakutud "biddingEndDate" segaseks, sest juhendis on kirjas, et "Peale toote �biddingEndDate�is� m�rgitud aja m��dumist API toodet enam ei paku",
aga minu kasutamise hetkel n�idati ainult l�ppenuid pakkumisi. N�ide: "biddingEndDate": "2018-07-17T18:37:42Z", aga DateTime.Now = 2018-07-17T21:35:33Z". 
D�naamilise men�� lahendasin selliselt, et kuvan kategooriaid ning kategooria valimisel peab vajutama "Filter" ning kui on soov filter eemaldada, siis on vaja valida "All" ja
uuesti vajutada "Filter".


